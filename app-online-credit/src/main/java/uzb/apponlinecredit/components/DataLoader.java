package uzb.apponlinecredit.components;

import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uzb.apponlinecredit.controller.CreditGiveController;
import uzb.apponlinecredit.entity.Credit;
import uzb.apponlinecredit.entity.Role;
import uzb.apponlinecredit.entity.User;
import uzb.apponlinecredit.entity.enums.Gender;
import uzb.apponlinecredit.entity.enums.RoleName;
import uzb.apponlinecredit.repository.CreditRepository;
import uzb.apponlinecredit.repository.RoleRepository;
import uzb.apponlinecredit.repository.UserRepository;

import java.text.SimpleDateFormat;
import java.util.HashSet;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    CreditRepository creditRepository;


    @Value("${spring.datasource.initialization-mode}")
    private String initMode;

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {

            roleRepository.save(new Role(1, RoleName.ROLE_SUPER_ADMIN));
            roleRepository.save(new Role(2, RoleName.ROLE_ADMIN));
            roleRepository.save(new Role(3, RoleName.ROLE_USER));

            SimpleDateFormat dateFor = new SimpleDateFormat("dd.MM.yyyy");

            userRepository.save(
                    new User(
                            "SuperAdmin",
                            "Adminov",
                            "Adminovich",
                            "+998901234567",
                            passwordEncoder.encode("123"),
                            2000000.0,
                            "AB6276224",
                            Gender.MEN,
                            "###",
                            dateFor.parse("08.07.2017"),
                            dateFor.parse("09.07.2027"),
                            dateFor.parse("01.01.2000"),
                            "Tashkent city",
                            "Uzbek",
                            new HashSet<>(roleRepository.findAllByRoleName(RoleName.ROLE_SUPER_ADMIN)),
                            true
                    )
            );
        }
    }
}
