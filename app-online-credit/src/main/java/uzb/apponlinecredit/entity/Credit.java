package uzb.apponlinecredit.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Credit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //Credit nomi
    @Column(nullable = false)
    private String nameCredit;

    //Credit summasi
    @Column(nullable = false)
    private Double sum;

    //Credit foizi
    @Column(nullable = false)
    private double percent;

    //Credit qancha oy davomida to'lashi
    private double monthly;
}
