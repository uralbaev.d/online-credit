package uzb.apponlinecredit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.apponlinecredit.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByPassport(String passport);

    Optional<User> findByPhoneNumber(String phoneNumber);
}
