package uzb.apponlinecredit.service;

import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.apponlinecredit.entity.Credit;
import uzb.apponlinecredit.entity.User;
import uzb.apponlinecredit.payload.ApiResponse;
import uzb.apponlinecredit.repository.CreditRepository;
import uzb.apponlinecredit.repository.UserRepository;

import java.text.DecimalFormat;
import java.util.Optional;
import java.util.UUID;

@Service
public class CreditGiveService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    CreditRepository creditRepository;

    /**
     *Credit berish uchun method
     */
    public ApiResponse giveCredit(User user, Credit credit) {
        Optional<User> optional = userRepository.findById(user.getId());
        Optional<Credit> creditId = creditRepository.findById(credit.getId());
        if (!creditId.isPresent())
            return new ApiResponse("No credit ID", false);

        if (optional.isPresent()) {
            Double salary = user.getSalary();
            Double sumCredit = credit.getSum();
            double percent = credit.getPercent();
            double monthly = credit.getMonthly();

            DecimalFormat decimalFormat = new DecimalFormat("#,###,##0.0");
            decimalFormat.setGroupingUsed(true);
            Double yearSalaryWithTax = ((((salary * 12) * 70) / 100));
            Double fullCreditWithPercent = ((sumCredit * percent) / 100) + sumCredit;
            Double enoughMoney = (((monthly / 12) * yearSalaryWithTax));
            if (enoughMoney >= fullCreditWithPercent) {
                return new ApiResponse("Sorovingiz qabul qilindi. Credit va foizi " + decimalFormat.format(fullCreditWithPercent), true);
            } else {
                Double changeSumCredit = enoughMoney - (fullCreditWithPercent - enoughMoney);
                return new ApiResponse("Sizga boshqasini taklif etamiz: "+decimalFormat.format(changeSumCredit), true);
            }
        }
        return new ApiResponse("Error", false);
    }
}
